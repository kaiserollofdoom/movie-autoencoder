"""
Load tag data as NP arrays
"""
import pandas as pd


def load_and_return_genome(genome_tags_path: str, genome_scores_path:str) -> pd.DataFrame:
    """
    Load and return a denormed set of tags
    :param genome_tags_path:
    :param genome_scores_path:
    :return:
    """
    genome_scores = pd.read_csv(genome_scores_path)
    genome_tags = pd.read_csv(genome_tags_path)
    genome_scores = genome_scores.join(genome_tags, on='tagId', lsuffix='_l')
    genome_scores = genome_scores.drop(['tagId_l', 'tagId'], axis=1)
    pivoted_genome = genome_scores.pivot_table(index='movieId', columns=['tag'])
    return pivoted_genome


