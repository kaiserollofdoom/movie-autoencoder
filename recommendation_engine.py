import pandas as pd
import numpy as np
import torch
import click
from tqdm import tqdm

from autoencoder.tag_based_autoencoder import AutoEncoder
from load_data.load_tag_data import load_and_return_genome
from scipy.spatial.distance import cosine as cosine_similarity


def train_autoencoder(genome) -> AutoEncoder:
    autoencoder = AutoEncoder(genome, validation_perc=0.33)
    autoencoder.train(100, 128)
    autoencoder = autoencoder.set_to_eval()
    return autoencoder


def evaluate_movie_id(movieId: int, autoencoder: AutoEncoder, blank_genome:pd.DataFrame,movies:pd.DataFrame, top_k: int = 20) -> pd.DataFrame:
    def encode(genome_values):
        torch_tensor = torch.from_numpy(np.asarray([genome_values])).type(torch.FloatTensor)
        return autoencoder.encoder(torch_tensor.cuda()).cpu().detach().numpy()

    movie_genome = blank_genome[blank_genome.index == movieId].drop(['title', 'genres'], axis=1).iloc[0].values
    tqdm.pandas(desc='my bar')
    movie_genome_encode = encode(movie_genome)
    blank_genome['recommendations'] = blank_genome.drop(['title', 'genres'], axis=1).progress_apply(
        lambda x: cosine_similarity(movie_genome_encode, encode(x)), axis=1)
    print(blank_genome.sort_values(by='recommendations')[['title', 'genres']].head(top_k).to_markdown())
    # Better printing to the console
    del blank_genome['recommendations']
    return blank_genome


def find_movie(movie_title_query: str, movies:pd.DataFrame):
    print(movies[movies.title.str.contains(movie_title_query, case=False)].head(10)['title'].to_markdown())


@click.command()
def recommendation():
    genome = load_and_return_genome('/home/mrkaiser/Documents/code/movie_clustering/ml-25m/genome-tags.csv',
                                    '/home/mrkaiser/Documents/code/movie_clustering/ml-25m/genome-scores.csv')
    movies = pd.read_csv('/home/mrkaiser/Documents/code/movie_clustering/ml-25m/movies.csv')
    print(">> Training")
    autoencoder = train_autoencoder(genome)
    title_genome = genome.join(movies.set_index('movieId'))
    # ask for a movie query
    while True:
        movie_query = click.prompt('Search for a movie', type=str)
        find_movie(movie_query, title_genome)
        if not click.confirm('Search for another movie?'):
           movie_id = click.prompt("Please enter a movie_id", type=int)
           top_k = click.prompt("Please enter the number of recommendations", type=int, default=20)
           evaluate_movie_id(movie_id, autoencoder, title_genome, movies, top_k)
           click.confirm('Find more recommendations?', abort=True)


if __name__ == '__main__':
    recommendation()
