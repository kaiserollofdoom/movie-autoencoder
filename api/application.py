from flask import Flask, url_for, render_template
from flask_sqlalchemy import SQLAlchemy
from werkzeug.utils import redirect

application = Flask(__name__, template_folder='.')
application.config['SQLALCHEMY_DATABASE_URI'] = ''
application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(application)


@application.route("/")
def index():
    # find a random movie and redirect
    random_movie_sql = """
    select *
    from
    (SELECT DISTINCT
    movie_id
    from recommendations)
    movies
    order
    by
    RANDOM()
    limit
    1
    """
    random_movie = db.engine.execute(random_movie_sql)
    random_movie_id = random_movie.fetchone()[0]
    return redirect(url_for("recommendation", movie_id=random_movie_id))


@application.route('/recommendation/<int:movie_id>')
def recommendation(movie_id: int):
    recommendation_query = f"""select *
    from
    (select r.recommendation_score, r.recommendation_id
    from
    movies as m
    join recommendations as r
    on
    m.movie_id = r.movie_id
    where
    m.movie_id = {movie_id}
    order by r.recommendation_score asc)
    recommendations
    join
    movies as m
    on
    m.movie_id = recommendation_id
    limit
    100;
    """
    recommendations = db.engine.execute(recommendation_query)
    # the first row is the movie we are making the recommendation on
    first_recommendation = recommendations.fetchone()
    movie_recommendation = dict(zip(first_recommendation.keys(), first_recommendation))
    results = [dict(zip(row.keys(), row)) for row in recommendations]
    return render_template('movie_results.html', movie_recommendation=movie_recommendation,
                           recommendations=results)
