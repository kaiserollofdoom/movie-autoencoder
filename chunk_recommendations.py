import pandas as pd
import numpy as np

recommendations = pd.read_csv("recommendations.csv")
recommendations.rename(columns={"Unnamed: 0": "movie_id"}, inplace=True)
recommendations.drop(columns=["title", "genres"], inplace=True)
recommendations_flat = recommendations.set_index("movie_id").unstack().reset_index().rename(
    columns={"level_0": "movie_id", "movie_id": "recommendation_id"})
number_of_chunks = 1000
for idx, chunk in enumerate(np.array_split(recommendations_flat, number_of_chunks)):
    print("CHUNK: {}".format(idx))
    chunk.to_csv('./output_data/recommendations_flat_{0}.csv'.format(idx))
