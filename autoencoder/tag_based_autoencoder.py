import copy
import multiprocessing

import pandas as pd
import numpy as np
import torch.nn as nn
from torch.optim import Adam
from torch.utils.data import Dataset, DataLoader
import torch
from tqdm import tqdm

from load_data.load_tag_data import load_and_return_genome
import matplotlib.pyplot as plt


def get_cv_idxs(n, cv_idx=0, val_pct=0.2, seed=42):
    """ Get a list of index values for Validation set from a dataset

    Arguments:
        n : int, Total number of elements in the data set.
        cv_idx : int, starting index [idx_start = cv_idx*int(val_pct*n)]
        val_pct : (int, float), validation set percentage
        seed : seed value for RandomState

    Returns:
        list of indexes
    """
    np.random.seed(seed)
    n_val = int(val_pct * n)
    idx_start = cv_idx * n_val
    idxs = np.random.permutation(n)
    return idxs[idx_start:idx_start + n_val]


def split_by_idx(idxs, *a):
    """
    Split each array passed as *a, to a pair of arrays like this (elements selected by idxs,  the remaining elements)
    This can be used to split multiple arrays containing training data to validation and training set.

    :param idxs [int]: list of indexes selected
    :param a list: list of np.array, each array should have same amount of elements in the first dimension
    :return: list of tuples, each containing a split of corresponding array from *a.
            First element of each tuple is an array composed from elements selected by idxs,
            second element is an array of remaining elements.
    """
    mask = np.zeros(len(a[0]), dtype=bool)
    mask[np.array(idxs)] = True
    return [(o[mask], o[~mask]) for o in a]


class TagEncoder(nn.Module):
    """
    The encoding step is a sequential process
    First do a linear transform and squeeze into a smaller dimensionality
    perform batch normalization on a 1d array
    do a ReLU activation
    do a dropout
    repeat to encoding size (so a further squeeze
    """

    def __init__(self, input_size: int, intermediate_size: int , encoding_size: int, dropout: float = None):
        super(TagEncoder, self).__init__()
        if not dropout:
            dropout = 0.2
        self.encoder = nn.Sequential(
            nn.Linear(input_size, intermediate_size),
            nn.BatchNorm1d(intermediate_size),
            nn.ReLU(True),
            nn.Dropout(dropout),
            nn.Linear(intermediate_size, encoding_size),
            nn.BatchNorm1d(encoding_size),
            nn.ReLU(True),
            nn.Dropout(dropout)
        )

    def forward(self, x):
        """
        The forward step for any pytorch module
        :param x:
        :return:
        """
        x = self.encoder(x)
        return x


class TagDecoder(nn.Module):
    """
    The decoding step is also a sequential process, going in  reverse of the Encoding step

    """

    def __init__(self, output_size: int, intermediate_size: int, encoding_size: int):
        super(TagDecoder, self).__init__()
        self.decoder = nn.Sequential(
            nn.Linear(encoding_size, intermediate_size),
            nn.BatchNorm1d(intermediate_size),
            nn.ReLU(True),
            nn.Dropout(0.2),
            nn.Linear(intermediate_size, output_size),
            nn.BatchNorm1d(output_size),
            nn.Sigmoid())

    def forward(self, x):
        x = self.decoder(x)
        return x


class AutoEncoderDataset(Dataset):

    def __init__(self, training_data: pd.DataFrame):
        self.x = training_data

    def __len__(self):
        return len(self.x)

    def __getitem__(self, idx):
        """
        Takes an index and returns the item from the dataset as a pytorch tensor
        :param idx:
        :return:
        """

        x = torch.from_numpy(self.x.iloc[idx].values).float()

        # return pair, remember the target for an autoencoder is the original value
        # we're trying to reconstruct the same value and minimize the lose between the two
        # we return a copy so that there are no reference issues
        return {'input': x, 'target': copy.deepcopy(x)}


class AutoEncoder(object):

    def __init__(self, data, validation_perc: float= 0.2, learning_rate: float=.001, intermediate_size: int=1000, encoded_size=100):
        # create a data loader to send data in batches
        self.data = data
        # TODO FIND SPLIT VALUES
        self.val_idxs = get_cv_idxs(n=data.shape[0], val_pct=validation_perc)
        [(self.val, self.train_set)] = split_by_idx(self.val_idxs, data)
        self.val = torch.from_numpy(self.val.values).type(torch.FloatTensor).cuda()
        dataset = AutoEncoderDataset(self.train_set)
        self.dataloader = DataLoader(dataset, batch_size=128, shuffle=True, num_workers=multiprocessing.cpu_count())
        size = data.shape[1]
        # setup the encode / decode steps
        self.encoder = TagEncoder(size, intermediate_size, encoded_size).cuda()
        self.decoder = TagDecoder(size, intermediate_size, encoded_size).cuda()

        # build the optimizers for the encode / decode steps
        self.encoder_optimizer = Adam(self.encoder.parameters(), lr=learning_rate, weight_decay=1e-8)
        self.decoder_optimizer = Adam(self.decoder.parameters(), lr=learning_rate, weight_decay=1e-8)

        # build the reconstruction loss.
        # Remember we are trying to minimize this criteria such that after the squeeze and the rebuild the values closely resemble the target
        self.reconstruction_loss = nn.MSELoss(reduction='mean')

        self.train_losses = []
        self.val_losses = []


    def _train_step(self, input, target):
        """
        perform a single training step
        :return:
        """

        self.encoder_optimizer.zero_grad()
        self.decoder_optimizer.zero_grad()

        #move forward on the encode / decode and calucate the reconstruction loss
        encoded_repr = self.encoder(input)
        reconstruction = self.decoder(encoded_repr)

        reconstruction_loss = self.reconstruction_loss(reconstruction, target)

        # calculate the gradient on the loss
        reconstruction_loss.backward()

        # move the encoders forward
        self.encoder_optimizer.step()
        self.decoder_optimizer.step()

        return reconstruction_loss.item()

    def _set_to_train(self):
        self.encoder.train()
        self.decoder.train()
        return self

    def set_to_eval(self):
        self.encoder.eval()
        self.decoder.eval()
        return self

    def get_val_loss(self, input, target):
        self.set_to_eval()
        encoded = self.encoder(input)
        val_reconstruction = self.decoder(encoded)
        val_reconstruction_loss = self.reconstruction_loss(val_reconstruction, target)
        return val_reconstruction_loss.item()

    def train(self, total_epochs, log_per_batch):
        for epoch in tqdm(range(total_epochs)):

            # enumerate the data loader to get the index of the batch and the batch itself

            for idx, batch in enumerate(self.dataloader):
                input = batch['input'].cuda()
                target = batch['target'].cuda()
                reconstruction_loss = self._train_step(input, target)


if __name__ == '__main__':
    genome = load_and_return_genome('/home/mrkaiser/Documents/code/movie_clustering/ml-25m/genome-tags.csv', '/home/mrkaiser/Documents/code/movie_clustering/ml-25m/genome-scores.csv')
    autoencoder = AutoEncoder(genome, validation_perc=0.33)
    autoencoder.train(50, 128)
    # plot the train
    plt.plot(range(len(autoencoder.train_losses)), autoencoder.train_losses, label='training loss')
    plt.plot(range(len(autoencoder.val_losses)), autoencoder.val_losses, label='validation loss')
    plt.legend()
    plt.savefig("Training Losses.png")



