from sqlalchemy import create_engine
import pandas as pd


def create_sql_alchemy_engine():
    return create_engine('')

engine = create_sql_alchemy_engine()
for chunk in range(0, 1001):
    df = pd.read_csv("./output_data/recommendations_flat_{}.csv".format(chunk))
    df.drop("Unnamed: 0", inplace=True, axis=1)
    df.rename(columns={"0": "recommendation_score"}, inplace=True)
    df.to_sql(name='recommendations', if_exists='append', con=engine)
