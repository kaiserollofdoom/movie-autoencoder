from multiprocessing.dummy import Manager
from typing import Tuple

import pandas as pd
import numpy as np
import torch
import click
from joblib import Parallel, delayed
from sqlalchemy import create_engine
from tqdm import tqdm

from autoencoder.tag_based_autoencoder import AutoEncoder
from load_data.load_tag_data import load_and_return_genome
from scipy.spatial.distance import cosine as cosine_similarity


def train_autoencoder(genome) -> AutoEncoder:
    autoencoder = AutoEncoder(genome, validation_perc=0.33)
    autoencoder.train(130, 128)
    autoencoder = autoencoder.set_to_eval()
    return autoencoder


def evaluate_movie_id(movieId: int, autoencoder: AutoEncoder, blank_genome:pd.DataFrame) -> Tuple[int, pd.Series]:
    def encode(genome_values):
        torch_tensor = torch.from_numpy(np.asarray([genome_values])).type(torch.FloatTensor)
        return autoencoder.encoder(torch_tensor.cuda()).cpu().detach().numpy()

    movie_genome = blank_genome[blank_genome.index == movieId].drop(['title', 'genres'], axis=1).iloc[0].values
    tqdm.pandas(desc='my bar')
    movie_genome_encode = encode(movie_genome)
    recommendations = blank_genome.drop(['title', 'genres'], axis=1).progress_apply(
        lambda x: cosine_similarity(movie_genome_encode, encode(x)), axis=1)
    return movieId, recommendations


def evaluate_all_ids():
    genome = load_and_return_genome('/home/mrkaiser/Documents/code/movie_clustering/ml-25m/genome-tags.csv',
                                    '/home/mrkaiser/Documents/code/movie_clustering/ml-25m/genome-scores.csv')
    movies = pd.read_csv('/home/mrkaiser/Documents/code/movie_clustering/ml-25m/movies.csv')
    print(">> Training")
    autoencoder = train_autoencoder(genome)
    title_genome = genome.join(movies.set_index('movieId'))
    manager = Manager()
    namespace = manager.Namespace()
    namespace.movies = title_genome
    rec_tuples = Parallel(n_jobs=6)(delayed(evaluate_movie_id)(movie_id, autoencoder, title_genome) for movie_id in list(title_genome.index))
    recommendations = pd.DataFrame()
    for idx, rec in rec_tuples:
        rec = rec.rename(idx).transpose()
        recommendations = recommendations.append(rec)
    recommendations = recommendations.join(movies.set_index('movieId'))
    recommendations.to_csv('recommendations.gz', compression={"method":"gzip", "archive_name": "recommendations.gz"})
    return recommendations


def clean_up_recommendations(recommendations:pd.DataFrame):
    recommendations.genres = recommendations.genres.str.split("|")
    recommendations['year'] = recommendations.title.str.extract(r'(\d{4})')
    recommendations.title = recommendations.title.str.replace('\s\(\d{4}\)', '', regex=True)
    return recommendations


def insert_into_postgresql(recommendations: pd.DataFrame, engine):
    movie_info = recommendations[['title', 'year', 'genres']]
    recommendation_info = recommendations.drop(['title', 'year', 'genres'], axis=1)
    # insert movie info
    #movie_info.to_sql(name='movies', index=True, con=engine)
    movie_info.to_csv("movie_info.csv")
    # reorient recommendation info into index, recommendation idx, value
    recommendation_info = recommendation_info.stack()
    #recommendation_info.to_sql(name='recommendations', index=True, con=engine)
    recommendation_info.to_csv("recommendations.csv")


def create_sql_alchemy_engine():
    return create_engine('')


if __name__ == '__main__':
    recommendations = evaluate_all_ids()
    cleaned_recommendations = clean_up_recommendations(recommendations)
    engine = create_sql_alchemy_engine()
    insert_into_postgresql(recommendations, engine)

